//
//  DateTextField.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 21/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class DateTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
        createDatePicker()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
        createDatePicker()
    }

    func datePickerValueChanged(sender : UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        text = dateFormatter.string(from: sender.date)
    }
    
    func createDatePicker() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: .valueChanged)
        inputView = datePicker
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(SignUpViewController.dismissPicker))
        inputAccessoryView = toolBar
    }
    
    func customize() {
        clipsToBounds = true
        tintColor = UIColor.orangebase
        borderStyle = .roundedRect
        layer.cornerRadius = bounds.height / 2
        textColor = UIColor.black
        backgroundColor = UIColor.lightgrey
        textColor = UIColor.orangebase
        keyboardExtension()
    }
    
    func keyboardExtension() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.orangebase
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        resignFirstResponder()
    }
}
