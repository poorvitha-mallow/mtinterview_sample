//
//  Button.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 12/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class Button: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    
    // MARK: - Custom methods
    
    func customize() {
        clipsToBounds = true
        titleLabel?.font = UIFont.textFont14()
        tintColor = UIColor.white
        setTitleColor(UIColor.orangebase, for: .normal)
        
    }

}
