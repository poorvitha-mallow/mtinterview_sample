//
//  ProfileTableViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 18/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit


class ProfileTableViewController: UITableViewController {
    
    @IBOutlet weak var genderCell: CustomTableViewCell!
    @IBOutlet weak var dateofBirthCell: CustomTableViewCell!
    @IBOutlet weak var contactNumberCell: CustomTableViewCell!
    @IBOutlet weak var addressCell: CustomTableViewCell!
    
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetch()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Custom methods
    
    /**Fetch from core data*/
    func fetch() {
        let object = CoreData.fetchCoreData()
        genderCell.textLabel?.text = "Gender"
        genderCell.detailTextLabel?.text = object.firstObject.value(forKey: "gender") as? String
        dateofBirthCell.textLabel?.text = "Date of Birth"
        dateofBirthCell.detailTextLabel?.text = object.firstObject.value(forKey: "dateofbirth") as? String
        contactNumberCell.textLabel?.text = "Contact Number"
        contactNumberCell.detailTextLabel?.text = object.firstObject.value(forKey: "phonenumber") as? String
        addressCell.textLabel?.text = "Address"
        addressCell.detailTextLabel?.text = object.firstObject.value(forKey: "address") as? String
    }
    
}
