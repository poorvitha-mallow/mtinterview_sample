//
//  SegmentControl.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 12/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class SegmentControl: UISegmentedControl {
    
    
    // MARK: - Initialization methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    
    // MARK: - Custom methods
    
    func customize() {
        tintColor = UIColor.orangebase
    }
}
