//
//  AllUsersTableViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 20/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class AllUsersTableViewController: UITableViewController {
    
    var array : [String] = []
    var pageNumber : Int? = 1
    var totalPages : Int? = nil
    var totalItems : Int? = nil
    var urlPageNumber : String? = nil
    
    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
        navigationItem.title = NSLocalizedString("USERS", comment: "USERS")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Custom methods
    
    func loadData() {
        let auth_token = UserDefault.getAuthToken()
        self.urlPageNumber = String(self.pageNumber!)
        guard let url = URL(string: constants.kPathForAllUsers+urlPageNumber!) else {
            return
        }
        let header = ["Authorization" : "Bearer "+auth_token!, "Accept" : "application/json"]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: nil, method: .get){ (validation,json,statuscode) in
            if validation {
                self.totalItems = json["count"].int
                self.totalPages = json["total_pages"].int
                for i in 0..<self.totalItems!{
                    let fname = json["all_users"][i]["first_name"].string!
                    self.array.append(fname)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array[indexPath.row]
        return cell
    }
    
    
    // MARK: - Scroll View Delegates
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            if self.pageNumber! <= self.totalPages!{
                self.pageNumber = self.pageNumber! + 1
                self.loadData()
            }
        }
    }
}
