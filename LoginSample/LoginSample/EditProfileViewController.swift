//
//  EditProfileViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 20/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let genderData = ["Male", "Female"]
    var auth_token : String? = nil
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstNameTextField: CustomTextField!
    @IBOutlet weak var lastNameTextField: CustomTextField!
    @IBOutlet weak var genderTextField: CustomTextField!
    @IBOutlet weak var dateOfBirthTextField: DateTextField!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var chooseProfilePicture: UIButton!
    @IBOutlet weak var addressTextField: CustomTextField!
    
    
    // MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewWillAppear(true)
        createPicker()
        loadValues()
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
        navigationItem.title = NSLocalizedString("EDIT PROFILE", comment: "EDIT PROFILE")
        saveButton.setTitle(NSLocalizedString("Save", comment: "Save"), for: .normal)
        profileImage = UIImageView.roundedProfile(image: profileImage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Action Methods
    
    /**Save changes in database*/
    @IBAction func saveButtonPressed(_ sender: Any) {
        updateInDatabase()
        saveButton.showLoading()
    }
    
    /**Choose profile picture from photo library*/
    @IBAction func chooseProfilePicture(_ sender: Any) {
        let imagepicker = UIImagePickerController()
        imagepicker.delegate = self
        imagepicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagepicker, animated: true, completion: nil)
    }
    
    
    // MARK: - Custom methods
    
    /**Update details in database*/
    func updateInDatabase() {
        var imageString : String? = nil
        guard let url = URL(string: constants.kBaseUrl) else {
            return
        }
        if profileImage.image != nil {
            if let base64String = UIImageJPEGRepresentation(profileImage.image!, 0.9)?.base64EncodedString(){
                imageString = base64String
            }
        } else {
            imageString = nil
        }
        let header = ["Authorization" : "Bearer "+auth_token!, "Accept" : "application/json"]
        let parameters = ["user" : ["first_name" : firstNameTextField.text!, "last_name" : lastNameTextField.text!,"gender" : genderTextField.text!, "dob" : dateOfBirthTextField.text!, "address" : addressTextField.text! , "avatar" : imageString] ]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .put){ (validation,json,statuscode) in
            if validation {
                self.updateInCoreData()
            }
        }
    }
    
    /**Update details in Core data*/
    func updateInCoreData() {
        var imageString : String? = nil
        let object = CoreData.fetchCoreData()
        object.firstObject.setValue(firstNameTextField.text, forKey: "firstname")
        object.firstObject.setValue(lastNameTextField.text, forKey: "lastname")
        object.firstObject.setValue(genderTextField.text, forKey: "gender")
        object.firstObject.setValue(addressTextField.text, forKey: "address")
        object.firstObject.setValue(dateOfBirthTextField.text, forKey: "dateofbirth")
        if profileImage.image != nil {
            if let base64String = UIImageJPEGRepresentation(profileImage.image!, 0.9)?.base64EncodedString(){
                imageString = base64String
            }
        } else {
            imageString = nil
        }
        object.firstObject.setValue(imageString, forKey: "profilepicture")
        do {
            try object.managedobjectcontext.save()
            saveButton.hideLoading()
            displayalert(title: NSLocalizedString("Profile Updated Successfully", comment: "Profile Updated Successfully"), message: "")
        } catch let error as NSError{
            print("Could not save \(error)")
        }
        
    }
    
    /**Fetch current user values from core data*/
    func loadValues() {
        let object = CoreData.fetchCoreData()
        genderTextField.text = object.firstObject.value(forKey: "gender") as? String
        dateOfBirthTextField.text = object.firstObject.value(forKey: "dateofbirth") as? String
        firstNameTextField.text = object.firstObject.value(forKey: "firstname") as? String
        lastNameTextField.text = object.firstObject.value(forKey: "lastname") as? String
        addressTextField.text = object.firstObject.value(forKey: "address") as? String
        let imageEncodeString = object.firstObject.value(forKey: "profilepicture") as? String
        auth_token = UserDefault.getAuthToken()
        if imageEncodeString != nil {
            if let decodedData = Data(base64Encoded: imageEncodeString!, options: .ignoreUnknownCharacters) {
                profileImage.image = UIImage(data: decodedData)
            }
        }
    }
    
    /**picker for gender*/
    func createPicker() {
        let picker = UIPickerView()
        picker.delegate = self
        genderTextField.inputView = picker
        let pickertoolBar = UIToolbar().ToolbarPiker(mySelect: #selector(EditProfileViewController.dismissPicker))
        genderTextField.inputAccessoryView = pickertoolBar
    }
    
    func dismissPicker() {
        view.endEditing(true)
    }
    
    
    
    // MARK: - Delegate methods
    
    /** Dismiss photo library **/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    /**Gender picker delegates*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField.text = genderData[row]
    }
}
