    //
    //  LoggedInViewController.swift
    //  LoginSample
    //
    //  Created by Poorvitha Yogendran on 17/07/17.
    //  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
    //
    
    import UIKit
    import Alamofire
    import SwiftyJSON
    
    class LoggedInViewController: BaseViewController, UITableViewDelegate {
        
        @IBOutlet weak var profileButton: Button!
        @IBOutlet weak var logOutButton: UIButton!
        @IBOutlet weak var showAllUsersButton: Button!
        
        
        // MARK: - View life cycle methods
        
        override func viewDidLoad() {
            super.viewDidLoad()
            viewControllerTitles()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        
        // MARK: - Action methods
        
        /**Log Out*/
        @IBAction func logOutButtonPressed(_ sender: Any) {
            userSignOut()
        }
        
        // MARK: - Custom methods
        
        /**Sign out*/
        func userSignOut() {
            let object = CoreData.fetchCoreData()
            let auth_token = UserDefault.getAuthToken()
            let refresh_token = UserDefault.getRefreshToken()
            
            let email = (object.firstObject.value(forKey: "email") as? String)!
            guard let url = URL(string: constants.kPathForLogOut) else {
                return
            }
            let header = ["Authorization" : "Bearer "+auth_token!, "Refresh" : refresh_token]
            let parameters = ["user" : ["login" : email]]
            
            AlamofireRequest.alamofireRequest(url: url, header: header as! [String : String], parameters: parameters, method: .delete){ (validation,json,statuscode) in
                if validation {
                    self.deleteUserInfo()
                    self.performSegue(withIdentifier: "unwindtoLoginViewController", sender: self)
                } 
            }
            
        }
        
        /**Delete user details from core data*/
        func deleteUserInfo()
        {
            UserDefault.removeToken()
            let object = CoreData.fetchCoreData()
            for objects in object.allUsers{
                object.managedobjectcontext.delete(objects)
            }
        }
        
        func viewControllerTitles() {
            profileButton.setTitle(NSLocalizedString("Profile", comment: "Profile"), for: .normal)
            profileButton.titleLabel?.font = UIFont.textFont(size: 20)
            logOutButton.setTitle(NSLocalizedString("Log Out", comment: "Log Out"), for: .normal)
            logOutButton.tintColor = UIColor.red
            logOutButton.titleLabel?.font = UIFont.textFont(size: 20)
            showAllUsersButton.setTitle(NSLocalizedString("View Users", comment: "View Users"), for: .normal)
            showAllUsersButton.titleLabel?.font = UIFont.textFont(size: 20)
            navigationItem.title = NSLocalizedString("LOGGED VIEW", comment: "LOGGED VIEW")
            navigationItem.hidesBackButton = true
        }
    }
