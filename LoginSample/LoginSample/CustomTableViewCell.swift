//
//  CustomTableViewCell.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 18/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        customize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        customize()
    }
    
    func customize() {
        textLabel?.textColor = UIColor.gray
        textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        detailTextLabel?.textColor = UIColor.orangebase
    }
    


}
