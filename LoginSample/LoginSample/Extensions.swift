//
//  Extensions.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 11/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import Foundation
import UIKit

private var activityIndicator : UIActivityIndicatorView!

extension UIFont {

    class func textFont(size : CGFloat) -> UIFont? {
        return UIFont(name: "Helvetica", size: size)
    }
    
    class func textFont14() -> UIFont? {
        return textFont(size: 14.0)
    }

}

extension UIColor {
    
    class var whitecolor: UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 1.0)
    }
    
    class var orangebase : UIColor {
        return UIColor(red: 255/255, green: 152/255, blue: 0/255, alpha: 1)
    }
    
    class var lightgrey : UIColor {
        return UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

    }
}

extension UIImageView {
    
    class func roundedProfile(image : UIImageView) -> UIImageView {
        image.layer.cornerRadius = 0.5 * image.bounds.size.width
        image.clipsToBounds = true
        return image
    }

}

extension CustomButton {

//    class func showLoading(button: CustomButton) -> CustomButton {
//        if (activityIndicator == nil) {
//            activityIndicator = createActivityIndicator(button)
//        }
//        showSpinning(button)
//        return button
//    }
//    
//    func hideLoading() {
//        activityIndicator.stopAnimating()
//    }
//    
//    private func createActivityIndicator(button: CustomButton) -> UIActivityIndicatorView {
//        let activityIndicator = UIActivityIndicatorView()
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.color = UIColor.lightGray
//        return activityIndicator
//    }
//    
//    func showSpinning() {
//        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//        self.addSubview(activityIndicator)
//        centerActivityIndicatorInButton()
//        activityIndicator.startAnimating()
//    }
//    
//    private func centerActivityIndicatorInButton() {
//        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
//        self.addConstraint(xCenterConstraint)
//        
//        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
//        self.addConstraint(yCenterConstraint)
//    }
}




