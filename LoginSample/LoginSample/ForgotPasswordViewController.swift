//
//  ForgotPasswordViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 17/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordViewController: BaseViewController {
    
    var email : String? = nil
    
    @IBOutlet weak var codeTextField: CustomTextField!
    @IBOutlet weak var resendMailButton: Button!
    @IBOutlet weak var resetPasswordButton: CustomButton!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var conformPasswordTextField: CustomTextField!
    
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllerTitles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.displayalert(title: NSLocalizedString("Mail successfully sent", comment: "Mail successfully sent"), message: NSLocalizedString("Please check the mail", comment: "Please check the mail"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Action methods
    
    /**Resent email to reset password*/
    @IBAction func resendEmailButtonPressed(_ sender: Any) {
        guard let url = URL(string: constants.kPathForForgotPassword) else {
            return
        }
        let header = ["Accept" : "application/json"]
        let parameters = ["user" : ["email" : email!]]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .post){ (validation,json,statuscode) in
            if validation {
                self.displayalert(title: NSLocalizedString("Mail sent", comment: "Mail sent"), message: NSLocalizedString("Please check the mail", comment: "Please check the mail"))
            }
        }
    }
    
    /**Password is reset*/
    @IBAction func resetPasswordButtonPressed(_ sender: Any) {
        if passwordTextField.text != conformPasswordTextField.text {
            displayalert(title: NSLocalizedString("Passwords does not match", comment: "Passwords does not match"), message: "")
        } else if (codeTextField.text?.isEmpty)! {
            displayalert(title: NSLocalizedString("Reset Token cannot be null", comment: "Reset Token cannot be null"), message: NSLocalizedString("Enter a valid token", comment: "Enter a valid token"))
        } else {
            resetPasswordButton.showLoading()
            guard let url = URL(string: constants.kPathForForgotPassword ) else {
                return
            }
            
            let header = ["Accept" : "application/json"]
            let parameters = ["user" : ["reset_password_token" : codeTextField.text!, "password" : passwordTextField.text!]]
            
            AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .put){ (validation,json,statuscode) in
                if validation {
                    self.resetPasswordButton.hideLoading()
                    self.displayalert(title: NSLocalizedString("Password reset", comment: "Password reset"), message: NSLocalizedString("Login to continue...", comment: "Login to continue..."))
                }
            }
        }
    }
    
    
    // MARK: - Custom methods
    
    func viewControllerTitles() {
        codeTextField.placeholder = NSLocalizedString("Enter the verification code", comment: "Enter the verification code")
        resendMailButton.setTitle(NSLocalizedString("Resend Email", comment: "Resent Email"), for: .normal)
        resetPasswordButton.setTitle(NSLocalizedString("Reset Password", comment: "Reset Password"), for: .normal)
        passwordTextField.placeholder = NSLocalizedString("New Password", comment: "New Password")
        conformPasswordTextField.placeholder = NSLocalizedString("Conform Password", comment: "Conform Password")
        navigationItem.title = NSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
    }
    
}
