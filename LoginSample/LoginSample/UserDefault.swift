//
//  UserDefault.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 25/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

let kAuthToken = "Auth_Token"
let kRefreshToken = "Refresh_Token"

class UserDefault: NSObject {

    private class func archive(inputData: Any, forKey: String) {
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: inputData)
        UserDefaults.standard.set(archiveData, forKey:forKey)
        synchronize()
    }
    
    private class func unArchiveData(forKey: String) -> Any? {
        if let archivedData = UserDefaults.standard.object(forKey: forKey) as? Data {
            let unArchivedData = NSKeyedUnarchiver.unarchiveObject(with: archivedData)
            return unArchivedData
        } else {
            return nil
        }
    }
    
    class func updateAuthToken(authToken: String) {
        archive(inputData: authToken, forKey: kAuthToken)
    }
    
    class func updateRefreshToken(refreshToken: String) {
        archive(inputData: refreshToken, forKey: kRefreshToken)
    }
    
    class func getAuthToken() -> String? {
        return unArchiveData(forKey: kAuthToken) as? String
    }
    
    class func getRefreshToken() -> String? {
        return unArchiveData(forKey: kRefreshToken) as? String
    }
    
    class func removeToken() {
        UserDefaults.standard.removeObject(forKey: kAuthToken)
        UserDefaults.standard.removeObject(forKey: kRefreshToken)
        synchronize()
    }
    
    class func synchronize() {
        UserDefaults.standard.synchronize()
    }

}
