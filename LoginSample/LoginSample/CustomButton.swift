//
//  CustomButton.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 12/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    var activityIndicator : UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
        layer.cornerRadius = bounds.size.height / 2
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    
    // MARK: - Custom methods
    
    func customize() {
        titleLabel?.font = UIFont.textFont14()
        tintColor = UIColor.white
        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = UIColor.orangebase

    }
    
    func showLoading() {
        if (activityIndicator == nil) {
            activityIndicator = createActivityIndicator()
        }
        showSpinning()
    }
    
    func hideLoading() {
        activityIndicator.stopAnimating()
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.black
        return activityIndicator
    }
    
    func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }

   
}
