//
//  SignUpViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 11/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON


class SignUpViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let genderData = ["Male", "Female"]
    let manager = CLLocationManager()
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var chooseProfilePicButton: Button!
    @IBOutlet weak var firstNameTextfield: CustomTextField!
    @IBOutlet weak var lastNameTextField: CustomTextField!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var genderTextField: CustomTextField!
    @IBOutlet weak var dateOfBirthTextField: DateTextField!
    @IBOutlet weak var mobileNumberTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var conformPasswordTextField: CustomTextField!
    @IBOutlet weak var signUpButton: CustomButton!
    @IBOutlet weak var mobileCode: UIButton!
    @IBOutlet weak var verifyEmail: Button!
    
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllerTitles()
        createPicker()
        customAddressLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Action methods
    
    /**Select profile image from photo library*/
    @IBAction func chooseProfilePictureAction(_ sender: Any) {
        let imagepicker = UIImagePickerController()
        imagepicker.delegate = self
        imagepicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagepicker, animated: true, completion: nil)
    }
    
    /**Register user*/
    @IBAction func signUpButtonPressed(_ sender: Any) {
        if (firstNameTextfield.text?.isEmpty)! || (lastNameTextField.text?.isEmpty)! || (emailTextField.text?.isEmpty)! || (passwordTextField.text?.isEmpty)! || (conformPasswordTextField.text?.isEmpty)! || (addressLabel.text?.isEmpty)! || (mobileNumberTextField.text?.isEmpty)! {
            displayalert(title: NSLocalizedString("Field values cannot be null", comment: "Field values cannot be null"), message: NSLocalizedString("All Fields required to be filled", comment: "All Fields required to be filled"))
        } else if !isValidMobileNumber(testStr: mobileNumberTextField.text!) {
            displayalert(title: NSLocalizedString("Invalid Contact Number", comment: "Invalid Contact Number"), message: NSLocalizedString("Enter a valid Mobile Number", comment: "Enter a valid Mobile Number"))
        } else if !isPasswordSame(password: passwordTextField.text! , confirmPassword : conformPasswordTextField.text!) {
            displayalert(title: NSLocalizedString("Passwords does not match", comment: "Passwords does not match"), message: NSLocalizedString("Re-enter the passwords", comment: "Re-enter the passwords"))
        } else {
            signUpButton.showLoading()
            signup()
        }
    }
    
    /**Email Verification*/
    @IBAction func verifyEmailButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "showemailverification", sender: self)
    }
    
    // MARK: - Custom methods
    
    /**Set titles*/
    func viewControllerTitles() {
        chooseProfilePicButton.setTitle(NSLocalizedString("Choose Profile Picture", comment: "Choose Profile Picture"), for: .normal)
        firstNameTextfield.placeholder = NSLocalizedString("First Name", comment: "First Name")
        lastNameTextField.placeholder = NSLocalizedString("Last Name", comment: "Last Name")
        emailTextField.placeholder = NSLocalizedString("Email", comment: "Email")
        genderTextField.placeholder = NSLocalizedString("Gender (Optional)", comment: "Gender (Optional)")
        dateOfBirthTextField.placeholder = NSLocalizedString("Date of Birth (Optional)", comment: "Date of Birth (Optional)")
        mobileNumberTextField.placeholder = NSLocalizedString("Mobile Number", comment: "Mobile Number")
        passwordTextField.placeholder = NSLocalizedString("Password", comment: "Password")
        conformPasswordTextField.placeholder = NSLocalizedString("Conform Password", comment: "Conform Password")
        verifyEmail.setTitle(NSLocalizedString("Verify Email", comment: "Verify Email"), for: .normal)
        signUpButton.setTitle(NSLocalizedString("Sign Up", comment: "Sign Up"), for: .normal)
        emailTextField.keyboardType = .emailAddress
        mobileNumberTextField.keyboardType = .phonePad
        navigationItem.title = NSLocalizedString("SIGNUP", comment: "SIGNUP")
        countryCode(button: mobileCode)
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
        profileImageView = UIImageView.roundedProfile(image: profileImageView)
    }
    
    /**picker for gender*/
    func createPicker() {
        let picker = UIPickerView()
        picker.delegate = self
        genderTextField.inputView = picker
        let pickertoolBar = UIToolbar().ToolbarPiker(mySelect: #selector(SignUpViewController.dismissPicker))
        genderTextField.inputAccessoryView = pickertoolBar
    }
    
    func customAddressLabel() {
        addressLabel.textColor = UIColor.lightGray
        addressLabel.backgroundColor = UIColor.lightgrey
        addressLabel.tintColor = UIColor.orangebase
        addressLabel.layer.cornerRadius = addressLabel.bounds.height / 2
        addressLabel.layer.masksToBounds = true
        addressLabel.text = NSLocalizedString("Address", comment: "Address")
        
        /** Enable tap function for label **/
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.tapFunction))
        addressLabel.isUserInteractionEnabled = true
        addressLabel.addGestureRecognizer(tap)
    }
    
    func dismissPicker() {
        view.endEditing(true)
    }
    
    /**Tap function calls location delegate methods*/
    func tapFunction(sender:UITapGestureRecognizer) {
        addressLabel.textColor = UIColor.orangebase
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    /**Sign up*/
    func signup() {
        var imageString : String? = nil
        guard let url = URL(string: constants.kBaseUrl ) else {
            return
        }
        if profileImageView.image != nil {
            if let base64String = UIImageJPEGRepresentation(profileImageView.image!, 0.9)?.base64EncodedString() {
                imageString = base64String
            }
        } else {
            imageString = nil
        }
        
        let headers    = ["Content-Type" : "application/json"]
        let parameter = ["user" : [ "email" : emailTextField.text!,"first_name" : firstNameTextfield.text!, "last_name" : lastNameTextField.text!,"gender" : genderTextField.text!, "phone_number" : mobileNumberTextField.text!, "dob" : dateOfBirthTextField.text! ,"password" : passwordTextField.text!, "address" : addressLabel.text! , "avatar" : imageString] ]
        AlamofireRequest.alamofireRequest(url: url, header: headers, parameters: parameter, method: .post){ (validation,json,statuscode) in
            if validation {
                self.signUpButton.hideLoading()
                self.displayalert(title: NSLocalizedString("Registered Successfully", comment: "Registered Successfully"), message: NSLocalizedString("Verify your mail", comment: "Verify your mail"))
            }
        }
    }
    
    
    // MARK: - Delegate methods
    
    /**Gender picker delegates*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField.text = genderData[row]
    }
    
    /** Reverse geocode for address label **/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(locations[0]) {(placemark, error) in
            if error != nil {
                return
            }
            else if let place = placemark?[0]{
                var addressString : String = ""
                if place.subThoroughfare != nil {
                    addressString = place.subThoroughfare! + " "
                }
                if place.thoroughfare != nil {
                    addressString = addressString + place.thoroughfare! + ", "
                }
                if place.postalCode != nil {
                    addressString = addressString + place.postalCode! + " "
                }
                if place.locality != nil {
                    addressString = addressString + place.locality! + ", "
                }
                if place.administrativeArea != nil {
                    addressString = addressString + place.administrativeArea! + " "
                }
                if place.country != nil {
                    addressString = addressString + place.country!
                }
                self.addressLabel.text = addressString
                manager.stopUpdatingLocation()
                self.addressLabel.resignFirstResponder()
            }
        }
    }
    
    /** Address fetch fail with errors **/
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        displayalert(title: NSLocalizedString("Cant fetch location", comment: "Cant fetch location"), message: NSLocalizedString("Location fetch fail with errors", comment: "Location fetch fail with errors"))
    }
    
    /** Dismiss photo library **/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        profileImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
}

//Extension for done button in picker and date picker views
extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.orangebase
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
}



