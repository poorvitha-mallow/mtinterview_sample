//
//  AlamofireRequest.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 25/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class AlamofireRequest: NSObject {
    
    class func alamofireRequest(url : URL, header : [String : String], parameters : [String : Any]?, method : HTTPMethod, completionHandler: @escaping (_ validation : Bool,_ json : JSON, _ statuscode : Int) -> Void) {
        var statuscode : Int? = nil
        var responseValidation : Bool = false
        var json : JSON? = nil
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header)
            .validate().responseJSON(completionHandler:{ response in
                statuscode = response.response?.statusCode
                if response.data != nil {
                    switch response.result{
                    case .success(_):
                        responseValidation = true
                        json = JSON(data: response.data!)
                        
                    case .failure(_):
                        json = JSON(data: response.data!)
                        if statuscode == 440{
                            alamofireAuthTokenRequest()
                        }
                        break
                    }
                    completionHandler (responseValidation, json!, statuscode!)
                }
            })
    }
    
    class func updateAuthToken(auth_token : String){
        let object = CoreData.fetchCoreData()
        object.firstObject.setValue(auth_token, forKey: "authtoken")
        do {
            try object.managedobjectcontext.save()
        } catch let error as NSError{
            print("Could not save \(error)")
        }
    }
    
    class func alamofireAuthTokenRequest(){
        guard let url = URL(string: constants.kPathForGettingAuthToken ) else {
            return
        }
        let object = CoreData.fetchCoreData()
        let refresh_token : String = (object.firstObject.value(forKey: "refreshtoken") as? String)!
        let header = ["Refresh": refresh_token, "Accept" : "application/json"]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header)
            .validate().responseJSON{ response in
                if response.data != nil {
                    switch response.result{
                    case .success(_):
                        let json = JSON(data: response.data!)
                        let auth_token = json["auth_token"].string
                        updateAuthToken(auth_token: auth_token!)
                        
                    case .failure(_):
                        let parsedata = JSON(data: response.data!)
                        let parseDict = parsedata.dictionaryObject
                        if let parseFirst = parseDict?.first {
                            let parseValue = parseFirst.value as? String
                            print(parseValue!)
                        }
                        break
                        
                    }
                }
        }
        
    }


}
