//
//  ProfileViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 18/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit


class ProfileViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    var array = ["Gender", "Date of Birth", "Contact Number", "Address"]
    var arrayValues : [String] = []
    
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var changePasswordButton: Button!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewControllerTitle()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetch()
    }
    
    
    // MARK: - Action methods
    
    /**Change user password*/
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "changepassword", sender: self)
    }
  
    @IBAction func editProfileBarButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "editprofile", sender: self)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for allViewController in viewControllers {
            if allViewController is LoggedInViewController {
                self.navigationController!.popToViewController(allViewController, animated: true)
            }
        }
    }
    
    
    // MARK: - Custom methods
    
    func ViewControllerTitle() {
        changePasswordButton.setTitle(NSLocalizedString("Change Password", comment: "Change Password"), for: .normal)
        changePasswordButton.tintColor = UIColor.red
        profilePicture.layer.cornerRadius = 0.5 * profilePicture.bounds.size.width
        profilePicture.clipsToBounds = true
        firstNameLabel.textColor = UIColor.orangebase
        lastNameLabel.textColor = UIColor.orangebase
        emailLabel.textColor = UIColor.orangebase
        navigationItem.title = NSLocalizedString("PROFILE", comment: "PROFILE")
        navigationItem.hidesBackButton = false
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
        profilePicture = UIImageView.roundedProfile(image: profilePicture)
        
        emailLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
    }
    
    /**Fetch user details from core data*/
    func fetch() {
        let object = CoreData.fetchCoreData()
        emailLabel.text = object.firstObject.value(forKey: "email") as? String
        firstNameLabel.text = object.firstObject.value(forKey: "firstname") as? String
        lastNameLabel.text = object.firstObject.value(forKey: "lastname") as? String
        arrayValues.append((object.firstObject.value(forKey: "gender") as? String)!)
        arrayValues.append((object.firstObject.value(forKey: "dateofbirth") as? String)!)
        arrayValues.append((object.firstObject.value(forKey: "phonenumber") as? String)!)
        arrayValues.append((object.firstObject.value(forKey: "address") as? String)!)
        let imageEncodeString = object.firstObject.value(forKey: "profilepicture") as? String
        if imageEncodeString != nil{
            if let decodedData = Data(base64Encoded: imageEncodeString!, options: .ignoreUnknownCharacters) {
                profilePicture.image = UIImage(data: decodedData)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell
        if arrayValues[indexPath.row] != "" {
            cell?.titleLabel.textColor = UIColor.orangebase
            cell?.detailLabel.textColor = UIColor.orangebase
            cell?.titleLabel.text = array[indexPath.row]
            cell?.detailLabel.text = arrayValues[indexPath.row]
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if #available(iOS 10.0, *) {
            super.traitCollectionDidChange(previousTraitCollection)
            if previousTraitCollection?.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory{
                emailLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
            }
        } else {
            // Fallback on earlier versions
        }
    }
}
