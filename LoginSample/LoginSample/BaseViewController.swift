//
//  BaseViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 14/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import CoreData

struct constants {
    static let kBaseUrl = "https://jwt-api-rails.herokuapp.com/users"
    static let kPathForLogin = "https://jwt-api-rails.herokuapp.com/users/sign_in"
    static let kPathForEmailVerification = "https://jwt-api-rails.herokuapp.com/users/confirmation?confirmation_token="
    static let kPathForEmailVerificationResend = "https://jwt-api-rails.herokuapp.com/users/confirmation"
    static let kPathForForgotPassword = "https://jwt-api-rails.herokuapp.com/users/password"
    static let kPathForChangePassword = "https://jwt-api-rails.herokuapp.com/users/change_password"
    static let kPathForAllUsers = "https://jwt-api-rails.herokuapp.com/home?page="
    static let kPathForLogOut = "https://jwt-api-rails.herokuapp.com/users/sign_out"
    static let kPathForGettingAuthToken = "https://jwt-api-rails.herokuapp.com/get_auth_token"
}

class BaseViewController: UIViewController {
    
    var allusers : [NSManagedObject] = []
    
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Custom methods
    
    /**Check if text is a valid mobile number*/
    func isValidMobileNumber(testStr: String) -> Bool {
        let PHONE_REGEX = "^\\d{100}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        return result
    }
    
    /**Display alert*/
    func displayalert(title: String, message: String) {
        let alertcontroller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alertcontroller, animated: true, completion: nil)
        let action = UIAlertAction(title: "Ok",style: .default){(action: UIAlertAction) in }
        action.setValue(UIColor.orangebase, forKey: "titleTextColor")
        alertcontroller.addAction(action)
    }
    
    func displaychangealert(title: String, message: String) {
        let alertcontroller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alertcontroller, animated: true, completion: nil)
        let action = UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: .default, handler: {
            alert -> Void in
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for allViewController in viewControllers {
                if allViewController is LoginViewController {
                    self.navigationController!.popToViewController(allViewController, animated: true)
                }
            }
        })

        action.setValue(UIColor.orangebase, forKey: "titleTextColor")
        alertcontroller.addAction(action)
    }
    
    /**Check if password and conform password are same*/
    func isPasswordSame(password: String , confirmPassword: String) -> Bool {
        if password == confirmPassword {
            return true
        } else {
            return false
        }
    }
    
    /**Button to display country code*/
    func countryCode(button: UIButton) {
        button.clipsToBounds = true
        button.tintColor = UIColor.white
        button.backgroundColor = UIColor.orangebase
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.isUserInteractionEnabled = false
        button.setTitle("+91", for: .normal)
    }
    
    /**Date picker selected value*/
    func isDatePickerValueChanged(sender: UIDatePicker) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.string(from: sender.date)
    }
}
