//
//  ChangePasswordViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 19/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ChangePasswordViewController: BaseViewController {
    
    
    @IBOutlet weak var currentPasswordTextField: CustomTextField!
    @IBOutlet weak var newPasswordTextField: CustomTextField!
    @IBOutlet weak var conformPasswordTextField: CustomTextField!
    @IBOutlet weak var changePasswordButton: CustomButton!
    
    var isscuccess: Bool = false
    
    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        viewControllerTitles()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Action events
    
    /**Change password*/
    @IBAction func resetPasswordButtonPressed(_ sender: Any) {
        if (currentPasswordTextField.text?.isEmpty)! || (newPasswordTextField.text?.isEmpty)! || (conformPasswordTextField.text?.isEmpty)! {
            displayalert(title: NSLocalizedString("Enter all fields", comment: "Enter all fields"), message: "")
        } else if !isPasswordSame(password: newPasswordTextField.text!, confirmPassword: conformPasswordTextField.text!) {
            displayalert(title: NSLocalizedString("Passwords does not match", comment: "Passwords does not match"), message: NSLocalizedString("Re-enter the passwords", comment: "Re-enter the passwords"))
        } else {
            
            changePassword()
            changePasswordButton.showLoading()
        }
    }
    
    
    // MARK: - Custom methods
    
    /**Password reset*/
    func changePassword() {
        let auth_token = UserDefault.getAuthToken()
        guard let url = URL(string: constants.kPathForChangePassword) else {
            return
        }
        let header = ["Authorization" : "Bearer "+auth_token!, "Accept" : "application/json"]
        let parameters = ["user" : ["password" : newPasswordTextField.text!, "current_password" : currentPasswordTextField.text!]]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .put){ (validation,json,statuscode) in
            if validation {
                print(json)
                self.changePasswordButton.hideLoading()
                self.deleteUserInfo()
                self.displaychangealert(title: NSLocalizedString("Password Changed Successfully", comment: "Password Changed Successfully"), message: NSLocalizedString("Login with New Password", comment: "Login with New Password"))
            }
        }
    }
    
    func pop() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for allViewController in viewControllers {
            if allViewController is LoginViewController {
                self.navigationController!.popToViewController(allViewController, animated: true)
            }
        }
    }
    

    func deleteUserInfo()
    {
        UserDefault.removeToken()
        let object = CoreData.fetchCoreData()
        for objects in object.allUsers{
            object.managedobjectcontext.delete(objects)
        }
        do {
            try object.managedobjectcontext.save()
        } catch let error as NSError{
            print("Could not save \(error)")
        }
    }

    
    func viewControllerTitles() {
        currentPasswordTextField.placeholder = NSLocalizedString("Current Password", comment: "Current Password")
        newPasswordTextField.placeholder = NSLocalizedString("New Password", comment: "New Password")
        conformPasswordTextField.placeholder = NSLocalizedString("Conform Password", comment: "Conform Password")
        changePasswordButton.setTitle(NSLocalizedString("Reset Password", comment: "Reset Password"), for: .normal)
        navigationItem.title = NSLocalizedString("CHANGE PASSWORD", comment: "CHANGE PASSWORD")
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
    }
}
