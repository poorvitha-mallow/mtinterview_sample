//
//  CoreData.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 25/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import CoreData

class CoreData: NSObject {

    class func fetchCoreData() -> (managedobjectcontext : NSManagedObjectContext, firstObject : NSManagedObject, allUsers : [NSManagedObject]) {
        var alluser : [NSManagedObject] = []
        var firstObject : NSManagedObject? = nil
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        let managedobjectcontext = appdelegate?.managedObjectContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserDetails")
        do {
            alluser = try managedobjectcontext?.fetch(fetchrequest) as! [NSManagedObject]
            firstObject = alluser.first
        }catch let error as NSError{
            print("could not fetch \(error)")
        }
        return (managedobjectcontext!, firstObject!, alluser)
    }

}
