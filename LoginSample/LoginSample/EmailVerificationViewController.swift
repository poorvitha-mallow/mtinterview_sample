//
//  EmailVerificationViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 17/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EmailVerificationViewController: BaseViewController {
    
    @IBOutlet weak var emailTokenTextField: CustomTextField!
    @IBOutlet weak var verifyEmailButton: CustomButton!
    @IBOutlet weak var resendEmailButton: Button!
    @IBOutlet weak var emailTextField: CustomTextField!
    
    
    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllerTitles()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Action methods
    
    /**Email verifiction*/
    @IBAction func verifyButtonPressed(_ sender: Any) {
        if (emailTokenTextField.text?.isEmpty)! {
            displayalert(title: NSLocalizedString("Enter a valid Token", comment: "Enter a valid Token"), message: "")
        } else {
            verifyEmailButton.showLoading()
            guard let url = URL(string: constants.kPathForEmailVerification+emailTokenTextField.text!) else {
                return
            }
            let header = ["Accept" : "application/json"]
            AlamofireRequest.alamofireRequest(url: url, header: header, parameters: nil, method: .get) { (validation,json,statuscode) in
                if validation {
                    self.verifyEmailButton.hideLoading()
                    self.displayalert(title: NSLocalizedString("Email Verified Successfully", comment: "Email Verified Successfully"), message: NSLocalizedString("Login to Continue", comment: "Login to Continue"))
                    self.performSegue(withIdentifier: "unwindtoLoginViewController", sender: self)
                }
            }
        }
    }
    
    /**Resend mail for Verification*/
    @IBAction func resendEmailButtonPressed(_ sender: Any) {
        guard let url = URL(string: constants.kPathForEmailVerificationResend) else {
            return
        }
        let header = ["Accept" : "application/json"]
        let parameter = ["user" : ["email" : emailTextField.text!] ]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameter, method: .post){ (validation,json,statuscode) in
            if validation {
                self.displayalert(title: NSLocalizedString("Mail Sent", comment: "Mail Sent"), message: NSLocalizedString("Verify your Email", comment: "Verify your Email"))
            } 
        }
    }
    
    
    // MARK: - custom methods
    
    /**Set title for view controllers*/
    func viewControllerTitles() {
        verifyEmailButton.setTitle(NSLocalizedString("Verify Email", comment: "Verify Email"), for: .normal)
        resendEmailButton.setTitle(NSLocalizedString("Resend Email", comment: "Resend Email"), for: .normal)
        emailTokenTextField.placeholder = NSLocalizedString("Enter the code", comment: "Enter the code")
        emailTextField.placeholder = NSLocalizedString("Enter the Email", comment: "Enter the Email")
        navigationItem.title = NSLocalizedString("EMAIL VERIFICATION", comment: "EMAIL VERIFICATION")
        self.navigationController?.navigationBar.tintColor = UIColor.orangebase
    }
    
}
