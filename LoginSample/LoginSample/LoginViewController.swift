//
//  LoginViewController.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 11/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class LoginViewController: BaseViewController, UITextFieldDelegate {
    
    var email : String? = nil
    
    @IBOutlet weak var userEmailorNumbertextfield: CustomTextField!
    @IBOutlet weak var Passwordtextfield: CustomTextField!
    @IBOutlet weak var Loginbutton: CustomButton!
    @IBOutlet weak var Signupbutton: Button!
    @IBOutlet weak var forgotPasswordbutton: Button!
    @IBOutlet weak var loginSegmentControl: SegmentControl!
    @IBOutlet weak var mobileCode: UIButton!
    
    
    // MARK: - View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllerTitles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resignFirstResponder()
        mobileCode.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Action methods
    
    /**Login*/
    @IBAction func Loginbuttonpressed(_ sender: Any) {
        if (userEmailorNumbertextfield.text?.isEmpty)! || (Passwordtextfield.text?.isEmpty)! {
            displayalert(title: NSLocalizedString("Username or password cannot be null", comment: "Username or password cannot be null"), message: "")
        } else {
            
            self.login()
            self.Loginbutton.showLoading()
        }
    }
    
    /**Show signup view controller*/
    @IBAction func Signupbuttonpressed(_ sender: Any) {
        performSegue(withIdentifier: "Showsignup", sender: self)
    }
    
    /**Segmentation control*/
    @IBAction func loginSegmentpressed(_ sender: Any) {
        switch loginSegmentControl.selectedSegmentIndex {
        case 0 :
            userEmailorNumbertextfield.placeholder = NSLocalizedString("Email", comment: "Email")
            view.reloadInputViews()
            mobileCode.isHidden = true
        case 1 :
            userEmailorNumbertextfield.placeholder = NSLocalizedString("Mobile Number", comment: "Mobile Number")
            view.reloadInputViews()
            mobileCode.isHidden = false
            countryCode(button : mobileCode)
        default : break
        }
    }
    
    /**Forgot password*/
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        displayForgotPasswordAlert()
    }
    
    @IBAction func unwindtoLoginViewController(segue : UIStoryboardSegue){
        //unwind to login view controller
    }
    
    // MARK: - Custom methods
    
    /**Setting View Controller titles*/
    func viewControllerTitles() {
        Loginbutton.setTitle(NSLocalizedString("Login", comment: "Login"), for: .normal)
        Signupbutton.setTitle(NSLocalizedString("SignUp", comment: "SignUp"), for: .normal)
        Passwordtextfield.placeholder = NSLocalizedString("Password", comment: "Password")
        userEmailorNumbertextfield.placeholder = NSLocalizedString("Email", comment: "Email")
        forgotPasswordbutton.setTitle(NSLocalizedString("Forgot password?", comment: "Forgot password?"), for: .normal)
        loginSegmentControl.setTitle(NSLocalizedString("Email", comment: "Email"), forSegmentAt: 0)
        loginSegmentControl.setTitle(NSLocalizedString("Mobile", comment: "Mobile"), forSegmentAt: 1)
        navigationItem.title = NSLocalizedString("LOGIN", comment: "LOGIN")
    }
    
    /**Save to core data*/
    func savetoCoreDate(json : JSON) {
        let firstName = json["User"]["first_name"].string
        let lastName = json["User"]["last_name"].string
        let email = json["User"]["email"].string
        let phonenumber = json["User"]["phone_number"].string
        let address = json["User"]["address"].string
        let authToken = json["Tokens"]["Auth_token"].string
        let refreshToken = json["Tokens"]["Refresh_token"].string
        let picture = json["User"]["avatar"].string
        let dob = json["User"]["dob"].string
        let gender = json["User"]["gender"].string
        let managedcontext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        let user = NSEntityDescription.insertNewObject(forEntityName: "UserDetails", into: managedcontext)
        user.setValue(firstName, forKey: "firstname")
        user.setValue(lastName, forKey: "lastname")
        user.setValue(email, forKey: "email")
        user.setValue(gender, forKey: "gender")
        user.setValue(phonenumber, forKey: "phonenumber")
        user.setValue(address, forKey: "address")
        UserDefault.updateAuthToken(authToken: authToken!)
        UserDefault.updateRefreshToken(refreshToken: refreshToken!)
        user.setValue(dob, forKey: "dateofbirth")
        user.setValue(picture, forKey: "profilepicture")
        do {
            try managedcontext.save()
            self.performSegue(withIdentifier: "showloggedin", sender: self)
            savetoCoreDate(json: json)
        } catch let error as NSError{
            print("couldnt save \(error)")
        }
    }
    
    /**Login*/
    func login() {
        guard let url = URL(string: constants.kPathForLogin) else {
            return
        }
        let header = ["Accept" : "application/json"]
        let parameters = ["user" : ["login" : self.userEmailorNumbertextfield.text!, "password" : self.Passwordtextfield.text!]]
        AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .post){ (validation,json,statuscode) in
            if validation {
                print(json)
                for _ in json {
                    self.savetoCoreDate(json: json)
                }
                self.Loginbutton.hideLoading()
            }
        }
    }
    
    /**Alert for Forgot Password*/
    func displayForgotPasswordAlert() {
        let alertController = UIAlertController(title: NSLocalizedString("Forgot password", comment: "Forgot password"), message: "", preferredStyle: .alert)
        let submitAction = UIAlertAction(title: NSLocalizedString("Submit", comment: "Submit"), style: .default, handler: {
            alert -> Void in
            let emailTextField = alertController.textFields![0] as UITextField
            emailTextField.keyboardType = .emailAddress
            if (emailTextField.text?.isEmpty)! {
                self.displayalert(title: NSLocalizedString("Email cannot have Null value", comment: "Email cannot have Null value"), message: "")
            } else {
                self.email = emailTextField.text
                guard let url = URL(string: constants.kPathForForgotPassword) else {
                    return
                }
                let header = ["Accept" : "application/json"]
                let parameters = ["user" : ["email" : emailTextField.text!]]
                AlamofireRequest.alamofireRequest(url: url, header: header, parameters: parameters, method: .post){ (validation,json,statuscode) in
                    if validation {
                        self.performSegue(withIdentifier: "forgotpassword", sender: self)
                    } 
                }
            }
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = NSLocalizedString("Enter the registered EmailID", comment: "Enter the registered EmailID")
            textField.tintColor = UIColor.orangebase
        }
        alertController.addAction(submitAction)
        alertController.addAction(cancelAction)
        submitAction.setValue(UIColor.orangebase, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.orangebase, forKey: "titleTextColor")
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation methods
    
    /**Unwind to login screen*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forgotpassword" {
            let viewcontroller = segue.destination as? ForgotPasswordViewController
            viewcontroller?.email = email
        }
    }
}
