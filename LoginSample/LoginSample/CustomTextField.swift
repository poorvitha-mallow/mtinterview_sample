//
//  CustomTextField.swift
//  LoginSample
//
//  Created by Poorvitha Yogendran on 12/07/17.
//  Copyright © 2017 Poorvitha Yogendran. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITableViewDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
        borderStyle = .roundedRect
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    
    // MARK: - Custom methods
    
     func customize() {
        tintColor = UIColor.orangebase
        textColor = UIColor.black
        backgroundColor = UIColor.lightgrey
        textColor = UIColor.orangebase
        keyboardExtension()
    }
    
     func keyboardExtension() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.orangebase
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        returnKeyType = .next
        inputAccessoryView = doneToolbar
    }
    
    
    
    
     func doneButtonAction() {
        resignFirstResponder()
    }

}
